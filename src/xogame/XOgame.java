/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xogame;

import java.util.Scanner;

/**
 *
 * @author 66955
 */
public class XOgame {

    private char[][] board;
    private char Player;
    private int rows = 3;
    private int columns = 3;

    public static Scanner kb = new Scanner(System.in);

    public XOgame() {
        board = new char[rows][columns];
        Player = 'X';
        StartBoard();
    }

    public void StartBoard() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                board[i][j] = '-';
            }
        }
    }

    public void ShowBoard() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public boolean isBoardFull() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public void changePlayer() {
        if (Player == 'O') {
            Player = 'X';

        } else {
            Player = 'O';
        }
    }

    public void updateBoard() {
        boolean validInput = false;      // if true, the input is valid
        do {
            if (Player == 'x') {
                System.out.println("Turn X");
                System.out.print("Please input row, col: ");
            } else {
                System.out.println("Turn O");
                System.out.print("Please input row, col: ");
            }
            int row = kb.nextInt() - 1;  // array index starts at 0 instead of 1
            int col = kb.nextInt() - 1;

            if (row >= 0 && row < rows && col >= 0 && col < columns
                    && board[row][col] == '-') {
                board[row][col] = Player;  // update board
                validInput = true;  // input okay, exit loop
            } else {
                System.out.println("Sorry, the move at ("
                        + (row + 1) + ", " + (col + 1)
                        + ") is not valid. Please try again");
            }
        } while (!validInput);  // repeat until input is valid
    }

    public boolean checkForWinner() {
        return (checkRows() || checkColumns() || checkDiagonals());
    }

    private boolean checkRows() {
        for (int i = 0; i < rows; i++) {
            if (board[i][0] != '-' && board[i][0] == board[i][1]
                    && board[i][0] == board[i][2]) {
                return true;
            }
        }
        return false;
    }

    // Loop through rows and see if any are winners.
    private boolean checkColumns() {
        for (int i = 0; i < columns; i++) {
            if (board[0][i] != '-' && board[0][i] == board[1][i]
                    && board[0][i] == board[2][i]) {
                return true;
            }
        }
        return false;
    }

    // Check the two diagonals to see if either is a win.
    private boolean checkDiagonals() {
        return (board[0][0] != '-' && board[0][0] == board[1][1]
                && board[0][0] == board[2][2])
                || (board[0][2] != '-' && board[0][2] == board[1][1]
                && board[0][2] == board[2][0]);

    }

    public String toString() {
        return ">>> " + Player + " win <<<";
    }

    public static void main(String[] args) {
        XOgame g = new XOgame();
        boolean gameStatus = false;
        System.out.println("Welcome to OX Game");
        do {
            g.ShowBoard();
            g.updateBoard();
            

            if (g.checkForWinner()) {
                g.ShowBoard();
                System.out.println(g.toString());
                gameStatus = true;
            }
            if (g.isBoardFull()) {
                g.ShowBoard();
                System.out.println(">>>DRAW<<<");
                gameStatus = true;
            }
            g.changePlayer();
        } while (gameStatus == false);

    }

}
